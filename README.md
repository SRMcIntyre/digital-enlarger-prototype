**Digital Enlarger Prototype**

***Computer Vision for Human Computer Interaction Final Project from Winter 2018***

In this repository is the python source code, a html page of the code running in juptyer notebook alongside some images produced,
and a folder of images saved that were edited using the enlarger.

The enlarger was constructed using a pico projector that projected an image onto a white base of cardstock. 
Underneath the cardstock was and infrared lamp that illuminated the base evenly. The movements made, by hands or other objects,
were captured by the infrared camera located by the pico projector. This simulated the exposure of photographic paper
yet instead of relying on the amount of light hitting on the base, we looked for the amount of shadow. This was achieved as we already
know the pixel values of the image and their relative positions on the base and the exposure time.

A short video of the project can be found [here](https://www.youtube.com/watch?v=INyDxYCSlk0&t=22s).