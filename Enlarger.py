
# coding: utf-8

# In[ ]:


# Enlarger Simulation


# In[1]:


# standard setup
get_ipython().run_line_magic('matplotlib', 'inline')
import numpy
import matplotlib.pyplot as plt
import cv2
import time

# useful helper function -- Not sure if I need it
from helper import imshow

# kernel
kernel = numpy.ones((5,5),numpy.uint8)

# camera setup
camera = cv2.VideoCapture(0)

# reduce frame size to speed it up
w = 640
camera.set(cv2.CAP_PROP_FRAME_WIDTH, w) 
camera.set(cv2.CAP_PROP_FRAME_HEIGHT, w * 3/4) 
ret, black = camera.read()
ret, white = camera.read()


# In[2]:


def getImage(image):
    image = cv2.flip(image,1)
    image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    image = cv2.bilateralFilter(image,9,75,75)
    return image


# In[3]:


def displayCount(image):
    image = image.copy()
    cv2.putText(image, str(t), (400, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
    #M = cv2.getRotationMatrix2D((col/2,row/2),270,1)
    #image = cv2.warpAffine(image,M,(col,row))
    cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("window",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
    cv2.imshow("window",image)


# In[4]:


def getDiff(image1,image2):
    
    img2 = cv2.add(image2,1)
    img1 = cv2.subtract(image1,img2)
    kernel = numpy.ones((5,5),numpy.uint8)
    img1 = cv2.erode(img1,kernel,iterations = 3)
    
    ret,mask = cv2.threshold(img1,25,255,cv2.THRESH_BINARY)
    img1 = cv2.bitwise_or(img1,mask)
    img1 = 255-img1
    return img1


# In[5]:


# select image here
#1 = cv2.IMREAD_GRAYSCALE('image.jpg')
img1 = cv2.imread('image2.jpg',cv2.IMREAD_GRAYSCALE)
# project inverted version of it
image = (255-img1)
row, col= image.shape[:2]
bottom = image[row-2:row, 0:col]

bordersize = 10
image = cv2.resize(image,None,fx=.6, fy=.6, interpolation = cv2.INTER_CUBIC)
image = cv2.copyMakeBorder(image, top=10*bordersize, bottom=bordersize, left=bordersize, right=bordersize, borderType= cv2.BORDER_CONSTANT, value=[0,0,0] )

M = cv2.getRotationMatrix2D((col/2,row/2),270,1)
image = cv2.warpAffine(image,M,(col,row))


# In[6]:


# select image here
#1 = cv2.IMREAD_GRAYSCALE('image.jpg')
img1 = cv2.imread('image2.jpg',cv2.IMREAD_GRAYSCALE)
# project inverted version of it
img1 = (255-img1)
row, col= img1.shape[:2]
bottom = img1[row-2:row, 0:col]

bordersize = 10
image = cv2.copyMakeBorder(image, top=10*bordersize, bottom=bordersize, left=10*bordersize, right=10*bordersize, borderType= cv2.BORDER_CONSTANT, value=[0,0,0] )

# get outline mask & prompt for paper in place
ret, frame = camera.read()
w, h = frame.shape[:2]
while True:
    # get frame
    ret, frame = camera.read()
    
    # get frame size
    
    w, h = frame.shape[:2]
    frame = getImage(frame)
    # mirror the frame
    #frame = cv2.flip(frame, 1)
    
    #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("window",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
    cv2.imshow("window",image)
    #cv2.imshow("frame",frame)
    # exit on ESC press
    if cv2.waitKey(5) == 27:
        black = frame
        break

cv2.imshow("black",black)
crop_img = black[50:w-50, 50:h-50]
bordersize = 50
crop_img = cv2.copyMakeBorder(crop_img, top=bordersize, bottom=bordersize, left=bordersize, right=bordersize, borderType= cv2.BORDER_CONSTANT, value=[0,0,0] )
ret,mask = cv2.threshold(crop_img,1,255,cv2.THRESH_BINARY)
poop = cv2.bitwise_and(crop_img,mask)
plt.imshow(crop_img, cmap="gray")
plt.show()
plt.imshow(poop,cmap="gray")
plt.show()

while True:
    # exit on ESC press
    if cv2.waitKey(5) == 27:
        while True:
            ret, frame = camera.read()
            w, h = frame.shape[:2]
            # mirror the frame
            white = getImage(frame)
            
            cv2.imshow("white",white)
            if cv2.waitKey(5) == 27:
                break
        break
        
white = cv2.add(white,1)
kernel = numpy.ones((5,5),numpy.uint8)
plt.imshow(black,cmap="gray")
plt.show()

cv2.destroyAllWindows()
#cv2.imshow(black)



# In[7]:


# time
import time 
import cv2

import matplotlib.pyplot as plt
exposureTime = 10
count = 0
#image = black.astype('float')


# In[8]:


#plt.imshow(image)
#plt.show()

while True:
    
    cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("window",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
    cv2.imshow("window",image)
    if cv2.waitKey(5) == 27:
        break
        

cv2.destroyAllWindows()
endTime = time.time() + exposureTime
white = white*0
white = white.astype('uint32')

while True:
    #cv2.putText(image, str(endTime - time.time()), (0, 0), cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 0, 255), 5)
    t = round(endTime-time.time(),1)  
    displayCount(image)
    # get frame
    ret, frame = camera.read()
    # get frame size
    w, h = frame.shape[:2]
    frame = getImage(frame)
    #cv2.imshow("Frame",frame)
    if cv2.waitKey(5)==27:
        break
    if (time.time() > endTime):
        break
    frame = getDiff(poop,frame)
    white = white + frame
    count = count + 1
    
cv2.destroyAllWindows()


# In[9]:


print (white)
img1 = cv2.imread('image2.jpg',cv2.IMREAD_GRAYSCALE)
plt.imshow(img1,cmap="gray")
plt.show()
# project inverted version of it
img1 = (255-img1)
whiteTotal = (white/(100.0*count))
whiteTotal = cv2.resize(whiteTotal,(row+200, col+200), interpolation = cv2.INTER_CUBIC)
crop_img = cv2.resize(crop_img,(row+200, col+200), interpolation = cv2.INTER_CUBIC)
ret,croppy = cv2.threshold(crop_img,1,255,cv2.THRESH_BINARY)

#whiteTotal = cv2.bitwise_and(whiteTotal,croppy)

floaty = whiteTotal*255.0/(exposureTime*count)
print(floaty.max())
M = cv2.getRotationMatrix2D(((col+200)/2,(row+200)/2),180,1)
floaty = cv2.warpAffine(floaty,M,(col+100,row+100))
bordersize = 50
realImage = cv2.copyMakeBorder(img1, top=bordersize, bottom=bordersize, left=bordersize, right=bordersize, borderType= cv2.BORDER_CONSTANT, value=[0,0,0] )

realImage = floaty*realImage
realImage = numpy.clip(realImage, 0, 255)
realImage = realImage.astype('uint8')
plt.imshow(img1,cmap="gray")
plt.show()
plt.imshow(realImage,cmap="gray")
plt.show()
finalImage = 255-realImage
plt.imshow(finalImage,cmap="gray")
plt.show()

cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
cv2.setWindowProperty("window",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
cv2.imshow("window",finalImage)

